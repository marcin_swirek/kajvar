const menu = document.querySelector('.menu-container');
const innerWidth = window.innerWidth;
const orderBtn = document.querySelector('.footer-order-link-one');
const showBtn = document.querySelectorAll('.menu-show-btn');

//Menu uncovering handler

const showMenu = e => {
	const index = e.target.dataset.key;
	console.log(index);
	document.querySelector(
		`.menu-show-container[data-key='${index}'`
	).style.opacity = '0';
};

//Contact order handler
orderBtn.addEventListener('click', () => {
	document.querySelector('.footer-contact').classList.toggle('active');
});

//Carousel toggle
const carouselToggle = () => {
	if (window.innerWidth >= 1024) {
		menu.classList.add('owl-carousel');
	} else {
		menu.classList.remove('owl-carousel');
	}
};

//Owl carousel settings
if ($(window).width() >= 1024) {
	$(document).ready(function() {
		$('.owl-carousel').owlCarousel({
			loop: false,
			margin: 5,
			nav: true,
			merge: true,
			responsive: {
				1024: {
					items: 3,
					mergeFit: true
				},
				1500: {
					items: 4,
					mergeFit: true
				}
			}
		});
	});
}
//==============================================================

carouselToggle();
showBtn.forEach(btn => btn.addEventListener('click', showMenu));
// window.addEventListener('resize', liveInnerWidth);
